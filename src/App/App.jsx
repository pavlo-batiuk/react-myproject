import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import SignUpForm from "../pages/auth/SignUp";
import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <Route path="/" component={SignUpForm} />
      </Router>
    </div>
  );
}

export default App;
