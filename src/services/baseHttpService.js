import axios from 'axios';
import _ from 'lodash';
import { baseUrlAddress } from '../utils/vars';

function baseRequest(params) {

  const axiosInstance = axios.create({
    baseURL: baseUrlAddress,
    xsrfCookieName: 'csrftoken',
    xsrfHeaderName: 'X-CSRFToken',
    headers: {
      Accept: 'application/json',
    },
  });

  axiosInstance.interceptors.response.use(
    response => response,
    async (error) => {
      const errors = {
        statusCode: _.get(error, 'response.status'),
        ..._.get(error, 'response.data', error),
      };

      return Promise.reject(errors);
    },
  );

  return axiosInstance({
    method: _.get(params, 'method'),
    url: _.get(params, 'url'),
    data: _.get(params, 'data', {}),
  });
}

async function getRequest(url) {
  const result = await baseRequest({
    method: 'GET', url
  });

  return _.get(result, 'data', {});
}

async function postRequest(url, data) {
  const result = await baseRequest({
    method: 'POST', url, data
  });

  return _.get(result, 'data', {});
}

async function putRequest(url, data) {
  const result = await baseRequest({
    method: 'PUT', url, data
  });

  return _.get(result, 'data', {});
}

async function deleteRequest(url, data) {
  const result = await baseRequest({
    method: 'DELETE', url, data
  });

  return _.get(result, 'data', {});
}

const baseHttpService = {
  get: getRequest,
  post: postRequest,
  put: putRequest,
  delete: deleteRequest,
};

export default baseHttpService;