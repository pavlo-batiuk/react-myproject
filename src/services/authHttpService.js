import tokenProvider from 'axios-token-interceptor';

const instance = axios.create({
  baseURL: process.env.HTTP_URL
});

// Configure the provider with the necessary options.
const options = { };
instance.interceptors.request.use(tokenProvider(options));

// When a call to an endpoint is made, a token will be provided as a header.
instance.get('/foo')