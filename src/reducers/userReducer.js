import { UserActions } from '../actions/userActions';

const initialUserData = {
  id: '',
  email: '',
  password: '',
  confirmPassword: '',
  first_name: '',
  last_name: '',
  is_male: '',
};

export default function userReducer(state = initialUserData, action) {
  switch (action.type) {
    case UserActions.SET_USER_GENERAL_INFO:

      return {
        ...state,
      };

    default:
      return state;
  }
}