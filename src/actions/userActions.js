const UserActions = {
  SET_USER_GENERAL_INFO: 'SET_USER_GENERAL_INFO',
};

const setUserGeneralInfo = userData => ({
  type: UserActions.SET_USER_GENERAL_INFO,
  payload: userData,
});

export {
  UserActions,
  setUserGeneralInfo,
};